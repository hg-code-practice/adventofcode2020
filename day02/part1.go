package main

import (
	"fmt"
	"log"
	"regexp"
	"strconv"

	"bitbucket.org/headgeekette/adventofcode2020/day02/fields"
	"bitbucket.org/headgeekette/adventofcode2020/reader"
)

func main() {
	entry, err := reader.GetInput("input.txt")
	if err != nil {
		log.Fatalf("failed to read input")
	}

	var count int
	for _, e := range entry {
		s := fields.GetFields(e)

		min, err := strconv.Atoi(s[0])
		max, err := strconv.Atoi(s[1])
		if err != nil {
			log.Fatalf("failed to read input")
		}

		aORb := regexp.MustCompile(s[2])
		matches := aORb.FindAllStringIndex(s[3], -1)
		if len(matches) >= min && len(matches) <= max {
			count++
		}
	}

	fmt.Printf("Number of valid passwords: %d\n", count)
}
