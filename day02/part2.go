package main

import (
	"fmt"
	"log"
	"regexp"
	"strconv"

	"bitbucket.org/headgeekette/adventofcode2020/day02/fields"
	"bitbucket.org/headgeekette/adventofcode2020/reader"
)

func main() {
	entry, err := reader.GetInput("input.txt")
	if err != nil {
		log.Fatalf("failed to read input")
	}

	var count int
	for _, e := range entry {
		s := fields.GetFields(e)

		pos1, err := strconv.Atoi(s[0])
		pos2, err := strconv.Atoi(s[1])
		if err != nil {
			log.Fatalf("failed to read input")
		}

		aORb := regexp.MustCompile(s[2])
		matches := aORb.FindAllStringIndex(s[3], -1)
		pos1valid := false
		pos2valid := false
		for _, item := range matches {
			if item[1] == pos1 {
				pos1valid = true
			}
			if item[1] == pos2 {
				pos2valid = true
			}
		}
		if pos1valid != pos2valid {
			count++
		}

	}

	fmt.Printf("Number of valid passwords: %d\n", count)
}
