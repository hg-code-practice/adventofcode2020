package fields

import "regexp"

func GetFields(s string) []string {
	a := regexp.MustCompile(`\ |\:\ |\-`)
	return a.Split(s, -1)
}
