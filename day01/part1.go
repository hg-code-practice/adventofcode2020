package main

import (
	"fmt"
	"log"
	"strconv"

	"bitbucket.org/headgeekette/adventofcode2020/reader"
)

func main() {

	entry, err := reader.GetInput("input.txt")
	if err != nil {
		log.Fatalf("failed to read input")
	}

	for x := range entry {
		for y := range entry {
			if y+x < len(entry) {
				first, err := strconv.Atoi(entry[x])
				second, err := strconv.Atoi(entry[x+y])
				if err != nil {
					log.Fatalf("failed to convert entry")
				}

				if first+second == 2020 {
					fmt.Printf("x: %d against y: %d\n", first, second)
					fmt.Printf("multiplied: %d\n", first*second)
				}
			}

		}
	}
}
