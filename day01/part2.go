package main

import (
	"fmt"
	"log"
	"strconv"

	"bitbucket.org/headgeekette/adventofcode2020/reader"
)

func main() {

	entry, err := reader.GetInput("input.txt")
	if err != nil {
		log.Fatalf("failed to read input")
	}

	for x := range entry {
		for y := range entry {
			for z := range entry {
				if y+x+z < len(entry) {
					first, err := strconv.Atoi(entry[x])
					second, err := strconv.Atoi(entry[x+y])
					third, err := strconv.Atoi(entry[x+y+z])
					if err != nil {
						log.Fatalf("failed to convert entry")
					}

					if first+second+third == 2020 {
						fmt.Printf("x: %d y: %d z: %d\n", first, second, third)
						fmt.Printf("multiplied: %d\n", first*second*third)
					}
				}
			}
		}
	}
}
