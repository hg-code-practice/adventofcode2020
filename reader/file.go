package reader

import (
	"bufio"
	"log"
	"os"
)

func GetInput(filename string) ([]string, error) {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatalf("failed to open")
		return nil, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	var entry []string
	for scanner.Scan() {
		entry = append(entry, scanner.Text())
	}

	return entry, nil
}
